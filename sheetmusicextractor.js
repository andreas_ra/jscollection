//load images from scrolling sheet
var imageElements = Array.from(document.getElementsByClassName("split-image"));

//replace body by new container
document.body.innerHTML = "<div class='container' style='background: white;overflow: hidden;'></div>";
var container = document.getElementsByClassName("container")[0];

//fill container with images
for (i = 1; i < imageElements.length; i++) {
    container.appendChild(imageElements[i]);
}
